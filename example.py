from random import random

from StrategyTester import StrategyTester, Position
import Preprocess

class StrategyRandom(StrategyTester):
    def initialize(self, start, **kwargs):
        pass

    def take_position(self, index, tp=0.01, sl=0.01, timeout=1, elevative=True, **kwargs):
        prob = random()
        if prob > 0.9:
            price = self.open[index+1]
            tp = (1+tp)*price
            sl = (1-sl)*price
            fund = 1*self.leverage

            position = Position(index+1, price, tp, sl, timeout, 0, True, fund, self.commission)
            return position
        elif prob < 0.1:
            price = self.open[index + 1]
            tp = (1 - tp) * price
            sl = (1 + sl) * price
            fund = 1 * self.leverage
            position = Position(index + 1, price, tp, sl, timeout, 0, True, fund, self.commission)
            return position
        else:
            return None


rates_frame = Preprocess.preprocess(Preprocess.get_data(broker="MT5", symbol="EURUSD", time_frame="1h"))
strategy = StrategyRandom(rates_frame)
strategy.start(sl=0.01, tp=0.01, timeout=10, elevative=True)
strategy.plot_profit()
strategy.draw_positions(0, 100)
