from abc import ABC, abstractmethod

import Preprocess
from Preprocess import save_chart
from Constants import Dataset

import numpy as np
import mplfinance as mpf
import plotly.graph_objects as go
import plotly.io as pio
pio.renderers.default = "browser"


class StrategyTester(ABC):

    def __init__(self, rates_frame, start_index=0, end_index=None, commission=0.0002, leverage=1, verbose=False, iran=False):
        if end_index is None:
            end_index = rates_frame.shape[0]-1
        self.rates_frame = rates_frame
        self.start_index = start_index
        self.end_index = end_index
        self.commission = commission
        self.leverage = leverage
        self.verbose = verbose
        self.open = rates_frame[Dataset.OPEN]
        self.close = rates_frame[Dataset.CLOSE]
        self.low = rates_frame[Dataset.LOW]
        self.high = rates_frame[Dataset.HIGH]
        self.body = rates_frame[Dataset.BODY]
        self.volume = rates_frame[Dataset.VOLUME]
        self.bullish = rates_frame[Dataset.IS_BULLISH]
        self.volatility = rates_frame[Dataset.VOLATILITY]
        self.time = rates_frame[Dataset.DATE]
        self.start_date = self.time[self.start_index]
        self.end_date = self.time[self.end_index - 1]
        self.closed_trades = []
        self.pending_trades = []
        self.last_trade = None
        self.profitable_count = 0
        self.max_open_buy = 0
        self.max_open_sell = 0
        self.max_open = 0
        self.timedout_count = 0
        self.profit = 0
        self.drawdown = 0
        self.money = 1
        self.sharpe = 0
        self.sortino = 0
        self.omega = 0
        self.cumulative_profit_over_time = []
        self.profit_over_time = []
        self.iran = iran
        if self.iran:
            self.dollar = Preprocess.preprocess(Preprocess.get_data("TGJU", "PRICE_DOLLAR_RL", "1D"))

    @property
    def unrealized_profit(self):
        profit = 0
        for trade in self.pending_trades:
            profit += trade.profit
        return profit / self.money

    def info(self, index=None):
        if index is None:
            index = self.end_index
        hold_time = 0
        for trade in self.closed_trades:
            hold_time += trade.closed_at - trade.index
        buy, sell = self._get_positions(0, index)
        total_months = (self.rates_frame['timestamp'][self.rates_frame.shape[0]-1] - self.rates_frame['timestamp'][0]) / (60*60*24*30*1000)
        candle_per_month = int(round(self.rates_frame.shape[0] / total_months))
        monthly = []
        for i in range(candle_per_month, len(self.cumulative_profit_over_time), candle_per_month):
            monthly.append((self.cumulative_profit_over_time[i] + 1) / (self.cumulative_profit_over_time[i-candle_per_month] + 1) - 1)
        risk_free = 0.00
        monthly = np.array(monthly)
        if len(monthly) > 1:
            mean_profit = monthly.mean()
            self.sharpe = (mean_profit * 12 - risk_free) / (monthly.std() * 12 ** (1 / 2))
            if len(monthly[monthly < 0]) > 1 and len(monthly[monthly > 0] > 1):
                self.sortino = (mean_profit * 12 - risk_free) / (monthly[monthly < 0].std() * 12 ** (1 / 2))
                self.omega = (monthly[monthly > mean_profit].sum() - mean_profit) / abs(mean_profit - monthly[monthly < mean_profit].sum())
        print(
            "index: {}, date:{}, profit: {:.2f}, money: {:.2f}\n"
            "open trades: {}: closed trades: {} ({} long, {} short), timed out: {}, profitable: {}, accuracy:{:.2f}\n"
            "drawdown: {:.2f}, RoMaD:{:.2f}, sharpe: {:.2f}, sortino: {:.2f}, omega: {:.2f}\n"
            "maximum open trades: {}, maximum open buy trades: {}, maximum open sell trades: {}, hold time:{}".format(
                index, self.rates_frame[Dataset.DATE][index], self.profit, self.money,
                len(self.pending_trades), len(self.closed_trades), len(buy), len(sell), self.timedout_count, self.profitable_count, self.profitable_count / max(len(self.closed_trades), 1),
                self.drawdown, (self.money**(12/max(len(monthly), 1)) - 1) / max(self.drawdown, 0.00001), self.sharpe, self.sortino, self.omega,
                self.max_open, self.max_open_buy, self.max_open_sell, hold_time))
        print("===============================================================")

    def _get_positions(self, start, end, closed=True, pending=True, verbose=False):
        buy = []
        sell = []
        positions = []
        if pending:
            positions += self.pending_trades
        if closed:
            positions += self.closed_trades
        for position in positions:
            if start <= position.index < end:
                if position.buy:
                    buy.append(position.index)
                else:
                    sell.append(position.index)
                if verbose:
                    print(position.profit)
        return buy, sell

    @abstractmethod
    def initialize(self, start, **kwargs):
        pass

    @abstractmethod
    def take_position(self, index, **kwargs):
        pass

    def start(self, **kwargs):
        self.rates_frame['position'] = ""
        self.rates_frame['position_fund'] = 0.
        self.rates_frame['money'] = 10000.
        self.initialize(self.start_index, **kwargs)
        print("test started")
        print("start:{}, end:{}".format(self.start_date, self.end_date))
        profit_max = 1
        if self.iran:
            if self.rates_frame[Dataset.TIMESTAMP][self.start_index] >= self.dollar[Dataset.TIMESTAMP][0]:
                dollar = self.dollar[self.dollar[Dataset.TIMESTAMP] <= self.rates_frame[
                    Dataset.TIMESTAMP][self.start_index]]
                last_dollar = dollar[Dataset.OPEN][dollar.shape[0] - 1]
            else:
                last_dollar = self.dollar[Dataset.OPEN][0]
        for i in range(self.start_index, self.end_index):
            if self.last_trade and self.last_trade.index == i:
                self.rates_frame.loc[i, 'position_fund'] = "long" if self.last_trade.buy else "short"
                self.rates_frame.loc[i, 'position_fund'] = self.last_trade.fund * 10000
            if self.verbose and i % 300 == 0:
                self.info(i)
            profit = self.evaluate_open_positions(i)
            self.money += profit
            if self.iran and self.rates_frame[Dataset.TIMESTAMP][i] > self.dollar[Dataset.TIMESTAMP][0]:
                dollar = self.dollar[self.dollar[Dataset.TIMESTAMP] <= self.rates_frame[
                    Dataset.TIMESTAMP][i]]
                self.money *= last_dollar / dollar[Dataset.OPEN][dollar.shape[0] - 1]
            self.profit += profit
            current_profit = self.money
            for pending in self.pending_trades:
                if self.iran and self.rates_frame[Dataset.TIMESTAMP][i] > \
                        self.dollar[Dataset.TIMESTAMP][0]:
                    pending.fund *= last_dollar / dollar[Dataset.OPEN][dollar.shape[0] - 1]
                current_profit += pending.profit
            if self.iran and self.rates_frame[Dataset.TIMESTAMP][i] > \
                    self.dollar[Dataset.TIMESTAMP][0]:
                last_dollar = dollar[Dataset.OPEN][dollar.shape[0] - 1]
            self.cumulative_profit_over_time.append(current_profit-1)
            open_buy, open_sell = self._get_positions(self.start_index, i, closed=False)
            self.max_open_buy = max(self.max_open_buy, len(open_buy))
            self.max_open_sell = max(self.max_open_sell, len(open_sell))
            self.max_open = max(self.max_open, len(self.pending_trades))
            profit_max = max(profit_max, self.money)
            self.drawdown = max(self.drawdown, 1 - current_profit / profit_max)
            self.rates_frame.loc[i, 'money'] = (self.cumulative_profit_over_time[-1] + 1) * 10000
            position = self.take_position(i, **kwargs)
            if position is not None:
                position.fund *= self.money
                self.pending_trades.append(position)
                self.last_trade = position
        print("final result:")
        self.money = self.cumulative_profit_over_time[-1] + 1
        self.info(self.end_index)

    def evaluate_open_positions(self, index):
        temp = []
        profit = 0
        for position in self.pending_trades:
            if not position.closed:
                position.evaluate_position(index, self.open[index], self.close[index], self.low[index], self.high[index])
            if position.closed:
                self.closed_trades.append(position)
                profit += position.profit
                self.profit_over_time.append(position.profit)
                if position.profit > 0:
                    self.profitable_count += 1
                if position.timedout and position.broken_tps == 0:
                    self.timedout_count += 1
            else:
                temp.append(position)
        self.pending_trades = temp
        return profit

    def plot_profit(self, start=None, end=None, log=True):
        if start is None:
            start = self.start_index
        if end is None:
            end = self.end_index
        date = self.time.to_list()[start:end]
        profit = np.array(self.cumulative_profit_over_time[start-self.start_index:end-self.start_index]) + 1
        close = list(self.close[start:end] / self.close[start])
        fig = go.Figure([go.Scatter(x=date, y=profit, name='Strategy')])
        fig.add_trace(go.Scatter(x=date, y=close, name='Buy and Hold'))
        fig.add_trace(
            go.Scatter(x=[date[end-start-1]], y=[profit[end-start-1]], mode='text', text=[round(profit[end-start-1], 3)], showlegend=False))
        fig.add_trace(
            go.Scatter(x=[date[end-start-1]], y=[close[end-start-1]], mode='text', text=[round(close[end-start-1], 3)], showlegend=False))
        fig.update_layout(height=700, width=1000)
        fig.update_xaxes(title_text='date')
        fig.update_yaxes(title='profit')
        if log:
            fig.update_yaxes(title_text='profit', type='log')
        fig.show()

    def draw_positions(self, start=None, end=None, draw_profit=True):
        if start is None:
            start = self.start_index
        if end is None:
            end = self.end_index
        buy, sell = self._get_positions(start, end, closed=True, pending=True)
        closes = []
        for trade in self.closed_trades:
            closes.append(trade.closed_at)
        if draw_profit:
            apd = [mpf.make_addplot(self.cumulative_profit_over_time[start-self.start_index:end-self.start_index], color='b')]
        else:
            apd = []
        save_chart(self.rates_frame, "positions", start, end, buy, sell, closes, additional_apds=apd)

    @staticmethod
    def mix(strategies, base=0, weights=None):

        class MixedStrategy(StrategyTester):

            def initialize(self, start, **kwargs):
                pass

            def take_position(self, index, **kwargs):
                pass

        if weights is None:
            weights = [1/len(strategies)] * len(strategies)
        weights = np.array(weights) / sum(weights)
        mixed_strategy = MixedStrategy(strategies[base].rates_frame, strategies[base].start_index, strategies[base].end_index)
        mixed_timestamps = np.array(mixed_strategy.rates_frame[Dataset.TIMESTAMP][
                           mixed_strategy.start_index:mixed_strategy.end_index])
        mixed_strategy.cumulative_profit_over_time = np.zeros(len(mixed_timestamps))
        mixed_strategy.money = 0
        for strategy, weight in zip(strategies, weights):
            mixed_strategy.closed_trades += strategy.closed_trades
            mixed_strategy.pending_trades += strategy.pending_trades
            mixed_strategy.profit += strategy.profit * weight
            mixed_strategy.money += strategy.money * weight
            mixed_strategy.timedout_count += strategy.timedout_count
            mixed_strategy.profit_over_time += strategy.profit_over_time
            mixed_strategy.profitable_count += strategy.profitable_count

            money = np.array(strategy.cumulative_profit_over_time) + 1
            timestamps = np.array(strategy.rates_frame[Dataset.TIMESTAMP][
                                  strategy.start_index:strategy.end_index])
            if len(money[timestamps < mixed_timestamps[0]]) > 0:
                money /= money[timestamps < mixed_timestamps[0]][-1]
            money *= weight
            for i in range(len(mixed_timestamps)):
                if len(money[timestamps <= mixed_timestamps[i]]) > 0:
                    mixed_strategy.cumulative_profit_over_time[i] += money[timestamps <= mixed_timestamps[i]][-1]
                else:
                    mixed_strategy.cumulative_profit_over_time[i] += weight
        profit_max = mixed_strategy.cumulative_profit_over_time[0]
        for i in range(len(mixed_timestamps)):
            profit_max = max(profit_max, mixed_strategy.cumulative_profit_over_time[i])
            mixed_strategy.drawdown = max(mixed_strategy.drawdown, 1 - mixed_strategy.cumulative_profit_over_time[i] / profit_max)
        mixed_strategy.cumulative_profit_over_time -= 1
        return mixed_strategy


class Position:
    def __init__(self, index, price, tp, sl, timeout, maker, elevative, fund, commission):
        self.index = index
        self.price = price
        self.timeout = timeout
        self.sl = sl
        if type(tp) != list:
            tp = [tp]
        self.tp_list = tp
        self.money_ratio = fund
        self.fund = fund
        self.broken_tps = 0
        self.profit = 0
        self.timedout = False
        self.closed = False
        self.closed_at = None
        self.closed_at_price= 0
        self.commission = commission
        self.elevative = elevative or len(self.tp_list) > 1
        self.maker = maker
        if self.tp_list[0] > sl:
            self.buy = True
        else:
            self.buy = False

    def evaluate_position(self, index, open, close, low, high):
        if self.closed:
            return
        self.calculate_profit(close)
        if self.maker:
            self.evaluate_maker(index, open, close, low, high)
        else:
            self.evaluate_taker(index, open, close, low, high)

    def evaluate_maker(self, index, open, close, low, high):
        if self.buy:
            if low <= self.sl:
                self.close(index, self.sl, False)
            elif high >= self.tp_list[0] and index > self.index:
                self.close(index, self.tp_list[0], False)
        else:
            if high >= self.sl:
                self.close(index, self.sl, False)
            elif low <= self.tp_list[0] and index > self.index:
                self.close(index, self.tp_list[0], False)
        if not self.closed and index >= self.index+self.timeout:
            target = round(open*(1+self.maker), 2)
            if target == open:
                raise Exception("target==open")
            if self.buy and high >= target:
                self.close(index, target, True)
            elif not self.buy and low <= target:
                self.close(index, target, True)

    def evaluate_taker(self, index, open, close, low, high):
        if index == self.index + self.timeout:
            self.close(index, open, True)
        elif index > self.index + self.timeout:
            raise Exception("position open after timeout")
        elif self.buy:
            if low <= self.sl:
                self.close(index, self.sl, False)
            else:
                if self.elevative:
                    if self.sl < self.price and close - self.price > self.price - self.sl:
                        self.sl = self.price + (self.price - self.sl)*0.5
                    broken_tps = 0
                    for tp in self.tp_list:
                        if close > tp:
                            broken_tps += 1
                            self.sl = tp
                            self.timeout = self.timeout * 2
                        else:
                            break
                    self.broken_tps += broken_tps
                    self.tp_list = self.tp_list[broken_tps:]
                else:
                    tp = self.tp_list[0]
                    if high >= tp:
                        self.broken_tps += 1
                        self.close(index, tp, False)

        else:
            if high >= self.sl:
                self.close(index, self.sl, False)
            else:
                if self.elevative:
                    if self.sl > self.price and self.price - close > self.sl - self.price:
                        self.sl = self.price + (self.price - self.sl)*0.5
                    broken_tps = 0
                    for tp in self.tp_list:
                        if close < tp:
                            broken_tps += 1
                            self.sl = tp
                            self.timeout = self.timeout * 2
                        else:
                            break
                    self.broken_tps += broken_tps
                    self.tp_list = self.tp_list[broken_tps:]
                else:
                    tp = self.tp_list[0]
                    if low <= tp:
                        self.broken_tps += 1
                        self.close(index, tp, False)

    def close(self, index, price, timedout):
        self.calculate_profit(price)
        self.timedout = timedout
        self.closed = True
        self.closed_at = index
        self.closed_at_price = price

    def calculate_profit(self, price):
        self.profit = price / self.price - 1 if self.buy else 1 - price / self.price
        self.profit -= self.commission*(2+self.profit)
        self.profit *= self.fund
