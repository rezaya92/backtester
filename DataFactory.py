from Constants import Dataset, Paths
import pandas as pd
import ta
from os import path

from tools import _timeframe_interval_to_timestamp


def save_ohlcv(ohlcv, broker, symbol, timeframe, name_suffix=None):
    filename = f'{broker}_{symbol}_{timeframe}'
    if name_suffix is not None:
        filename += f'_{name_suffix}'
    filename += ".csv"
    ohlcv.to_csv(path.join(Paths.DATASET, filename), index=None)


def read_ohlcv(broker, symbol, time_frame, name_suffix=None):
    filename = f'{broker}_{symbol}_{time_frame}'
    if name_suffix is not None:
        filename += f'_{name_suffix}'
    filename += ".csv"
    dataset_path = path.join(Paths.DATASET, filename)
    ohlcv = pd.read_csv(dataset_path)
    return ohlcv


def _adjust_close(ohlcv):
    ohlcv = ohlcv.copy()
    adjust = ohlcv[Dataset.ADJUSTED_CLOSE] / ohlcv[Dataset.CLOSE]
    ohlcv[Dataset.OPEN] *= adjust
    ohlcv[Dataset.HIGH] *= adjust
    ohlcv[Dataset.LOW] *= adjust
    ohlcv[Dataset.CLOSE] *= adjust
    return ohlcv


def _cleanse_time(ohlcv):
    ohlcv = ohlcv.copy()
    if Dataset.TIMESTAMP not in ohlcv.columns:
        ohlcv[Dataset.DATE] = pd.to_datetime(ohlcv[Dataset.DATE])
        ohlcv[Dataset.TIMESTAMP] = ohlcv[Dataset.DATE].astype('int64') // 10 ** 6
    elif len(str(ohlcv[Dataset.TIMESTAMP][0])) < 11:
        ohlcv[Dataset.TIMESTAMP] *= 10 ** 3

    if len(ohlcv[Dataset.TIMESTAMP].unique()) != ohlcv.shape[0]:
        _guess_timestamps(ohlcv)

    ohlcv[Dataset.DATE] = pd.to_datetime(ohlcv[Dataset.TIMESTAMP], unit='ms')
    return ohlcv


def _guess_timestamps(ohlcv):
    print("warning: guessing time")
    ohlcv = ohlcv.copy()
    current_ts = ohlcv[Dataset.TIMESTAMP][0]
    ts_idx_list = []
    for index, row in ohlcv.iterrows():
        last_ts = row[Dataset.TIMESTAMP]
        if last_ts == current_ts:
            ts_idx_list.append(index)
        else:
            interval = (last_ts - current_ts) // len(ts_idx_list)
            ohlcv.loc[ts_idx_list, Dataset.TIMESTAMP] = [current_ts + interval * i for i in
                                                         range(len(ts_idx_list))]
            ts_idx_list = [index]
            current_ts = last_ts
    return ohlcv


def _add_common_features(ohlcv, include_indicators=False):
    ohlcv = ohlcv.copy()
    ohlcv[Dataset.BODY] = abs(ohlcv[Dataset.OPEN] - ohlcv[
        Dataset.CLOSE])
    ohlcv[Dataset.IS_BULLISH] = ohlcv[Dataset.OPEN] < ohlcv[Dataset.CLOSE]
    ohlcv[Dataset.RETURN] = ohlcv[Dataset.CLOSE] / ohlcv[
        Dataset.OPEN] - 1
    ohlcv[Dataset.VOLATILITY] = (ohlcv[Dataset.RETURN]).ewm(span=100).std()
    if include_indicators:
        ohlcv = ta.add_all_ta_features(
            ohlcv, open=Dataset.OPEN, high=Dataset.HIGH, low=Dataset.LOW,
            close=Dataset.CLOSE, volume=Dataset.VOLUME)
    return ohlcv


def preprocess(ohlcv, include_indicators=False):
    ohlcv = _cleanse_time(ohlcv)
    ohlcv.index = ohlcv[Dataset.DATE]

    if Dataset.ADJUSTED_CLOSE in ohlcv.columns:
        ohlcv = _adjust_close(ohlcv)
    ohlcv = _add_common_features(ohlcv, include_indicators=include_indicators)

    return ohlcv


def aggregate_candles(ohlcv, index_list):
    new_timestamp = ohlcv.loc[index_list[0], Dataset.TIMESTAMP]
    new_open = ohlcv.loc[index_list[0], Dataset.OPEN]
    new_close = ohlcv.loc[index_list[-1], Dataset.CLOSE]
    new_high = max(ohlcv.loc[index_list, Dataset.HIGH].values)
    new_low = min(ohlcv.loc[index_list, Dataset.LOW].values)
    new_volume = sum(ohlcv.loc[index_list, Dataset.VOLUME].values)
    aggregated = {
                Dataset.TIMESTAMP: new_timestamp,
                Dataset.OPEN: new_open,
                Dataset.CLOSE: new_close,
                Dataset.HIGH: new_high,
                Dataset.LOW: new_low,
                Dataset.VOLUME: new_volume}
    return aggregated


def convert_ohlcv_timeframe(ohlcv, timeframe, offset=0):
    interval = _timeframe_interval_to_timestamp(timeframe)
    converted_list = []
    current_ts = ohlcv[Dataset.TIMESTAMP][0]
    ts_idx_list = []
    for index, row in ohlcv.iterrows():
        last_ts = row[Dataset.TIMESTAMP]
        if (last_ts - offset) // interval == (current_ts - offset) // interval:
            ts_idx_list.append(index)
        else:
            converted_dict = aggregate_candles(ohlcv, ts_idx_list)
            converted_list.append(converted_dict)
            ts_idx_list = [index]
            current_ts = last_ts
    converted_dict = aggregate_candles(ohlcv, ts_idx_list)
    converted_list.append(converted_dict)
    converted_df = pd.DataFrame(converted_list)
    return converted_df


class RatesFrame:
    """
    """
    def __init__(self, broker, symbol, timeframe, ohlcv=None, force_download=False):
        self.mask_date = None

        self.broker = broker
        self.symbol = symbol
        self.timeframe = timeframe

        if not force_download and ohlcv is None:
            try:
                ohlcv = read_ohlcv(self.broker, self.symbol, self.timeframe)
            except FileNotFoundError:
                force_download = True

        if force_download:
            from Api import download_klines
            ohlcv = download_klines(self.broker, self.symbol, self.timeframe)

        self._ohlcv = preprocess(ohlcv)

    @property
    def ohlcv(self):
        if self.mask_date is None:
            return self._ohlcv
        else:
            return self._ohlcv.loc[:self.mask_date, :]

    @property
    def open(self):
        return self.ohlcv[Dataset.OPEN]

    @property
    def high(self):
        return self.ohlcv[Dataset.HIGH]

    @property
    def low(self):
        return self.ohlcv[Dataset.LOW]

    @property
    def close(self):
        return self.ohlcv[Dataset.CLOSE]

    @property
    def volume(self):
        return self.ohlcv[Dataset.VOLUME]

    @property
    def volatility(self):
        return self.ohlcv[Dataset.VOLATILITY]

    @property
    def timestamp(self):
        return self.ohlcv[Dataset.TIMESTAMP]

    @property
    def body(self):
        return self.ohlcv[Dataset.BODY]

    @property
    def is_bullish(self):
        return self.ohlcv[Dataset.IS_BULLISH]

    @property
    def return_percentage(self):
        return self.ohlcv[Dataset.RETURN]

    @property
    def date(self):
        return self.ohlcv[Dataset.DATE]

    def last_date_before(self, date):
        past_dates = self.date[self.date <= date]
        if past_dates.shape[0] > 0:
            return past_dates.index[-1]
        else:
            return -1

    def first_date_after(self, date):
        past_dates = self.date[self.date >= date]
        if past_dates.shape[0] > 0:
            return past_dates.index[0]
        else:
            return -1

    def mask(self, end_date):
        self.mask_date = end_date

    def unmask(self):
        self.mask_date = None
