from datetime import datetime
import pytz
import dateparser
from Constants import TimeFrames, Dataset, Paths
import numpy as np
import mplfinance as mpf


def date_to_timestamp(date):
    if isinstance(date, int):
        return date
    epoch = datetime.utcfromtimestamp(0).replace(tzinfo=pytz.utc)
    if isinstance(date, str):
        date = dateparser.parse(date)
    if date.tzinfo is None or date.tzinfo.utcoffset(date) is None:
        date = date.replace(tzinfo=pytz.utc)
    return int((date - epoch).total_seconds() * 1000.0)


def _timeframe_interval_to_timestamp(timeframe):
    if timeframe == TimeFrames.ONE_MINUTE:
        return 60 * 1000
    elif timeframe == TimeFrames.FIVE_MINUTE:
        return 5 * 60 * 1000
    elif timeframe == TimeFrames.FIFTEEN_MINUTE:
        return 15 * 60 * 1000
    elif timeframe == TimeFrames.THIRTY_MINUTE:
        return 30 * 60 * 1000
    elif timeframe == TimeFrames.ONE_HOUR:
        return 60 * 60 * 1000
    elif timeframe == TimeFrames.FOUR_HOUR:
        return 4 * 60 * 60 * 1000
    elif timeframe == TimeFrames.ONE_DAY:
        return 24 * 60 * 60 * 1000
    elif timeframe == TimeFrames.ONE_WEEK:
        return 7 * 24 * 60 * 60 * 1000
    else:
        print("timeframe is not valid")


# todo: change to plotly
def save_chart(rates_frame, name, start=0, end=None, buy_signals=None, sell_signals=None, points_of_interest=None,
               volume=False, additional_apds=None, panel_ratios=(1,)):
    if additional_apds is None:
        additional_apds = []
    if end is None:
        end = rates_frame.shape[0]
    if sell_signals is None:
        sell_signals = set()
    else:
        sell_signals = set(sell_signals)
    if buy_signals is None:
        buy_signals = set()
    else:
        buy_signals = set(buy_signals)
    if points_of_interest is None:
        points_of_interest = set()
    else:
        points_of_interest = set(points_of_interest)
    rates_frame = rates_frame.loc[start:end-1, :]
    apds = []
    chart_path = Paths.CHARTS + name + ".jpg"
    if len(buy_signals) + len(sell_signals) + len(points_of_interest) != 0:
        buy_pointers = []
        sell_pointers = []
        interest_pointers = []
        buy_count = 0
        sell_count = 0
        interest_count = 0
        for i in range(start, end):
            if i not in buy_signals:
                buy_pointers.append(np.nan)
            else:
                buy_pointers.append(rates_frame[Dataset.LOW][i] * 0.9995)
                buy_count += 1
            if i not in sell_signals:
                sell_pointers.append(np.nan)
            else:
                sell_pointers.append(rates_frame[Dataset.HIGH][i] * 1.0005)
                sell_count += 1
            if i not in points_of_interest:
                interest_pointers.append(np.nan)
            else:
                interest_pointers.append(rates_frame[Dataset.HIGH][i] * 1.0008)
                interest_count += 1
        if buy_count > 0:
            apds.append(mpf.make_addplot(buy_pointers, scatter=True, markersize=5, marker='^'))
        if sell_count > 0:
            apds.append(mpf.make_addplot(sell_pointers, scatter=True, markersize=5, marker='v'))
        if interest_count > 0:
            apds.append(mpf.make_addplot(interest_pointers, scatter=True, markersize=5))
    apds += additional_apds
    rates_frame = rates_frame.rename(
        columns={Dataset.DATE: "Date", Dataset.OPEN: "Open", Dataset.CLOSE: "Close",
                 Dataset.HIGH: "High", Dataset.LOW: "Low", Dataset.VOLUME: "Volume"})
    rates_frame = rates_frame.set_index("Date")
    mpf.plot(rates_frame, addplot=apds, type='candle', volume=volume, figscale=1.25,
             savefig=dict(fname=chart_path, dpi=1080, quality=100), style='yahoo', panel_ratios=panel_ratios)
    print("saved chart as", name + ".jpg")


