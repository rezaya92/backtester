from StrategyTester import StrategyTester, Position
import Preprocess
import ta


class StrategyMacd(StrategyTester):

    def initialize(self, start, **kwargs):
        self.macd = ta.wrapper.MACD(self.close).macd()
        self.signal = ta.wrapper.MACD(self.close).macd_signal()

    def take_position(self, index, **kwargs):
        if self.macd[index] > 0 and self.signal[index] > 0:
            if len(self.pending_trades) > 0 and not self.pending_trades[-1].buy:
                self.pending_trades[-1].close(index, self.open[index+1])
            sl = 0
            tp = 10 ** 9
            if len(self.pending_trades) == 0:
                return Position(index + 1, self.open[index + 1], tp, sl, 10000, 0, False, self.leverage, self.commission)
        else:
            if len(self.pending_trades) > 0 and self.pending_trades[-1].buy:
                self.pending_trades[-1].close(index, self.open[index+1], True)


rates_frame = Preprocess.preprocess(Preprocess.get_data(broker="MT5", symbol="EURUSD", time_frame="1h"))
strategy = StrategyMacd(rates_frame)
strategy.start()
strategy.plot_profit()
strategy.draw_positions(0, 20000)
