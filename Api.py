from binance.client import Client
from datetime import datetime
from tools import date_to_timestamp, _timeframe_interval_to_timestamp
from Constants import Secrets, Dataset, Brokers
import pandas as pd
from DataFactory import save_ohlcv


def download_klines(broker, symbol, timeframe, start=datetime(2018, 1, 1), end=datetime(2021, 1, 1),
                    futures=False, save=False, name_suffix=None):
    print("downloading data")
    start = date_to_timestamp(start)
    end = date_to_timestamp(end)
    if broker == Brokers.BINANCE:
        ohlcv = download_binance_klines(symbol, timeframe, start, end, futures)
    else:
        raise Exception("Broker not Supported.")
    if save:
        if futures:
            if name_suffix is None:
                name_suffix = 'futures'
            else:
                name_suffix = f'futures_{name_suffix}'
        save_ohlcv(ohlcv, broker, symbol, timeframe, name_suffix)
    return ohlcv


def download_binance_klines(symbol, timeframe, start, end, futures=False):
    client = Client(Secrets.API_KEY, Secrets.SECRET_KEY)
    limit = 500
    if futures:
        get = client.futures_klines
    else:
        get = client.get_klines
    ohlcv = pd.DataFrame(get(symbol=symbol, interval=timeframe, startTime=start, endTime=end, limit=limit))
    interval = _timeframe_interval_to_timestamp(timeframe)
    last_timestamp = ohlcv.iloc[-1, 0]
    next_timestamp = interval + last_timestamp
    while next_timestamp < end:
        progress = (last_timestamp - start) / (end - start)
        print(f'{progress * 100 // 1}% downloaded.')
        try:
            new_data = get(symbol=symbol, interval=timeframe, startTime=next_timestamp, endTime=end, limit=limit)
        except Exception:
            continue
        ohlcv = ohlcv.append(pd.DataFrame(new_data), ignore_index=True)
        last_timestamp = ohlcv.iloc[-1, 0]
        next_timestamp = interval + last_timestamp
    ohlcv = cleanse_binance_klines(ohlcv)
    return ohlcv


def cleanse_binance_klines(ohlcv):
    ohlcv = ohlcv.iloc[:-1, :-1]
    del ohlcv[6]
    ohlcv = ohlcv.rename(
        columns={0: Dataset.TIMESTAMP, 1: Dataset.OPEN, 2: Dataset.HIGH,
                 3: Dataset.LOW, 4: Dataset.CLOSE, 5: Dataset.VOLUME,
                 7: Dataset.QUOT_VOL, 8: Dataset.COUNT, 9: Dataset.BUY_VOL,
                 10: Dataset.QUOT_BUY_VOL})
    return ohlcv
